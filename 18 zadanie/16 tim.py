'''
def F(n):

    print(n, end='')

    if n > 3:

        F(n // 2)

        F(n - 1).
'''

def f(n):
    print(n)
    if n > 3:
        f(n // 2)
        f(n-1)


print(f(7))