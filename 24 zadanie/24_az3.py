f = open("24 (18).txt")

a = f.read()

count = []

for i in range(26):
    count.append(0)

for i in range(2, len(a) - 1):
    if a[i - 1] == a[i - 2]:
        index = ord(a[i]) - ord("A")
        count[index] += 1

max = 0
max_index = 0

for i in range(len(count)):
    if count[i] > max:
        max = count[i]
        max_index = i

print(chr(max_index + ord("A")))