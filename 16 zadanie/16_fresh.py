
def f(x):
    if x == 1:
        return 1
    if x > 1:
        return f(x - 1) * 2


for x in range(500):
    if f(x) == 2:
        print(x)
        break

"""
mod  -> %
div -> //
"""