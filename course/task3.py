x = 7 * 729**6 + 6 * 81**9 + 3**14 - 90

s = ""

while x > 0:
    s += str(x % 9)
    x //= 9
s = s[::-1]

count = 0
for x in s:
    if int(x) % 2 == 0:
        count += 1

print(count)