def f(n, m):
    if m == 0:
        return n
    else:
        return f(m, n % m)

count = 0
for m in range(100, 1001):
    for n in range(100, 1001):
        if f(n, m) == 30:
            count += 1
print(count)