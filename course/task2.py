def m(n):
    sum = 0
    count = 0
    for i in reversed(range(2, n)):
        if n % i == 0:
            sum += i
            count += 1
        if count == 2:
            break
    if count == 2:
        return sum
    else:
        return 0


for x in range(11_000_000, 1000000000):
    if 0 < m(x) < 10000:
        print(m(x))