f = open("24 (14).txt")

a = f.read().splitlines()

count = []
for i in range(26):
    count.append(0)

min_count = 100000000000
min_str = ""

for i in range(len(a)):
    if a[i].count("G") < min_count:
        min_count = a[i].count("G")
        min_str = a[i]

for i in range(len(min_str)):
    count[ord(min_str[i]) - ord("A")] += 1

max_index = 0
max_count = 0
for i in range(len(count)):
    if count[i] >= max_count:
        max_count = count[i]
        max_index = i

print(chr(max_index + ord("A")))