import itertools

s = sorted("БАТЫР")

index = 1
for x in itertools.product(s, repeat=5):
    a = ''.join(x)
    if a.count("Ы") == 0 and "АА" not in a:
        print(index, a)
    index += 1