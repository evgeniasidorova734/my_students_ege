def unique(x):
    if x < 0 and abs(x) % 10 == 3:
        return True
    return False

f = open()
a = f.read().splitlines()
n, k = map(int, a[0].split())
a = a[1:]
a = list(map(int, a))

mins = [1000000 for i in range(k)]

count = 0
sum = 0
max_sum = -1000000

for x in a:
    sum += x
    if unique(x):
        count += 1
    ost = count % k

    if sum < mins[ost]:
        mins[ost] = sum
    elif sum - mins[ost] > max_sum:
        max_sum = sum - mins[ost]

print(max_sum)