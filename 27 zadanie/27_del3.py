f = open("inf_22_10_20_27b (1).txt")

a = f.read().splitlines()[1:]
sum = 0
dif1 = []
dif2 = []
for x in a:
    k, m = map(int, x.split())
    sum += min(k, m)
    dif = abs(k - m)
    if dif % 3 == 1:
        dif1.append(dif)
    elif dif % 3 == 2:
        dif2.append(dif)

dif1.sort()
dif2.sort()
print(sum, sum % 3)
print(dif1[:10])
print(dif2[:10])
