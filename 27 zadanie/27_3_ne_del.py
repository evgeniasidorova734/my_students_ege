a = []

min_dif = 10000000
sum = 0

for x in a:
    k, m = map(int, x.split())
    sum += min(k, m)
    dif = abs(k - m)
    if dif % 3 != 0 and dif < min_dif:
        min_dif = dif

if sum % 3 != 0:
    print(sum)
else:
    print(sum + min_dif)