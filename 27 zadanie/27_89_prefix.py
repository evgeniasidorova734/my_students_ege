f = open("27_B (5).txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))

first = {key: (0, 0) for key in range(89)}

max_sum = 0
min_len = 100000000
k = 89

sum = 0

for i in range(len(a)):
    sum += a[i]
    ost = a[i] % k
    if first[ost] == (0, 0):
        first[ost] = (sum, i)
    else:
        if sum - first[ost][0] > max_sum:
            max_sum = sum - first[ost][0]
            min_len = i - first[ost][1] + 1
        elif sum - first[ost][0] == max_sum:
            min_len = min(min_len, i - first[ost][1] + 1)

print(min_len)
