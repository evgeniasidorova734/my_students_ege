f = open("inf_26_04_21_27b.txt")
a = f.read().splitlines()
a = a[1:]
min_sum1 = 10000000
min_sum2= 10000000
min_sum3 = 10000000
sum_min = 0
sum_max = 0
c_1 = 0
c_0 = 0
for x in a:
    l, m = map(int, x.split())
    if l % 2 != 0:
        if max(l,m) % 2 != 0 and min(l,m) % 2 != 0 and (l + m) < min_sum1:
            min_sum1 = l + m
        elif max(l, m) % 2 == 0 and min(l, m) % 2 != 0 and (l + m) < min_sum2:
            min_sum2 = l + m
        elif max(l,m) % 2 != 0 and min(l,m) % 2 == 0 and (l + m) < min_sum3:
            min_sum3 = l + m
        sum_min += min(l, m)
        sum_max += max(l, m)
if sum_min %  2 == 0 and sum_max % 2 != 0:
    print(sum_min + sum_max)
if sum_min %  2 != 0 and sum_max % 2 == 0:
    if min_sum1  < min_sum2 + min_sum3:
        print(sum_min + sum_max - min_sum1)
    else:
        print(sum_min + sum_max - (min_sum2 + min_sum3))
if sum_min %  2 != 0 and sum_max % 2 != 0:
    if min_sum2  < min_sum1 + min_sum3:
        print(sum_min + sum_max - min_sum2)
    else:
        print(sum_min + sum_max - (min_sum3 + min_sum1))
if sum_min %  2 == 0 and sum_max % 2 == 0:
    if min_sum3  < min_sum1 + min_sum2:
        print(sum_min + sum_max - min_sum3)
    else:
        print(sum_min + sum_max - (min_sum2 + min_sum1))



