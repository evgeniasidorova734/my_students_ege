f = open("28133_B.txt")

a = f.read().splitlines()

a = a[1:]
a = list(map(int, a))

max_sum = -1
max_number = -1
max_number_2 = -1

for x in a:
    if x > max_number:
        max_number = x
        max_number_2 = -1
    else:
        if (x + max_number) % 120 == 0 and x + max_number > max_sum:
            max_sum = x + max_number
            max_number_2 = x

print(max_number, max_number_2)
