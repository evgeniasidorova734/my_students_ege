f = open("26-56.txt")
a = f.read().splitlines()
v, k, n = map(int, a[0].split())
a = a[1:]
a = list(map(int, a))
a.sort(reverse=True)

local = []
disks = [v for i in range(k)]
turn = 0
for x in a:
    if disks[turn] >= x:
        disks[turn] -= x
    else:
        flag = False
        for i in range(turn, k):
            if disks[i] >= x:
                disks[i] -= x
                flag = True
                break
        if not flag:
            for i in range(0, turn):
                if disks[i] >= x:
                    disks[i] -= x
                    flag = True
                    break
        if not flag:
            local.append(x)
    turn = (turn + 1) % 8

print(disks)
print(sum(local), len(local))


