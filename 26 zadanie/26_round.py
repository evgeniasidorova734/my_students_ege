f = open("26-56.txt")
a = f.read().splitlines()
k, v, m = map(int, a[0].split())
a = a[1:]

a = list(map(int, a))
a.sort(reverse=True)

storage = [k for i in range(v)]
other = []

index = 0
for x in a:
    print(index)
    if storage[index] > x:
        storage[index] -= x
    else:
        found = False
        index = index + 1 if index != v - 1 else 0
        for i in range(index, 8):
            if storage[index] > x:
                storage[index] -= x
                found = True
        if not found:
            other.append(x)

print(sum(other), len(other))
