def m(n):
    p = 1
    count = 0
    for x in range(2, n + 1):
        if n % x == 0:
            p *= x
            count += 1
        if count == 5:
            break
    if count < 5:
        return 0
    else:
        return p


for n in range(200000001, 300000000):
    if 0 < m(n) < n:
        print(m(n))
