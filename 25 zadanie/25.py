def f(x):
    for i in range(1, x):
        if x % i == 0 and i % 10 == 8 and i != 8:
            return i


for x in range(500001, 10000000):
    if f(x):
        print(x, f(x))