'''

РОВНО ДВА ДЕЛИТЕЛЯ СРЕДИ (11, 13, 17, 19)
'''

def f(x):
    count = 0
    if x % 11 == 0:
        count += 1
    if x % 13 == 0:
        count += 1
    if x % 17 == 0:
        count += 1
    if x % 19 == 0:
        count += 1
    if count == 2:
        return True
    else:
        return False


count = 0
min_number = 1000000
for x in range(22000, 33001):
    if f(x):
        count += 1
        if x < min_number:
            min_number = x


print(count, min_number)